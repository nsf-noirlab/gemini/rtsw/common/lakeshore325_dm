%define _prefix /gem_base
%define name lakeshore325_dm
%define repository gemdev
%define debug_package %{nil}
%define arch %(uname -m)
%define checkout %(git log --pretty=format:'%h' -n 1) 

#These global defines are added to prevent stripping
# symbols on vxWorks cross-compiled code
# Getting 'strip' to work is probably only needed for
# building a related debug sub-package
#
# But this prevents all the strip warnings
# mrippa 20120202
%global _enable_debug_package 0
%global debug_package %{nil}
%global __os_install_post /usr/lib/rpm/brp-compress %{nil}

Summary: %{name} Package, Lakeshore 325 support package
Name: %{name}
Version: 0.1.0
Release: 2%{?dist}
License: EPICS Open License
Group: Applications/Engineering
Source0: %{name}-%{version}.tar.gz
ExclusiveArch: %{arch}
Prefix: %{_prefix}
## You may specify dependencies here
BuildRequires: pydm
Requires: pydm
## Switch dependency checking off
# AutoReqProv: no

%description
This is the module %{name}.


%prep
%setup -q 

%build

%install
export DONT_STRIP=1
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/%{_prefix}/bin
mkdir -p $RPM_BUILD_ROOT/%{_prefix}/usr/share/lakeshore325
cp -r styleSheet.css $RPM_BUILD_ROOT/%{_prefix}/usr/share/lakeshore325
cp -r LakeShore325.ui $RPM_BUILD_ROOT/%{_prefix}/usr/share/lakeshore325
cp -r *.sh $RPM_BUILD_ROOT/%{_prefix}/bin


%postun
if [ "$1" = "0" ]; then
	rm -rf %{_prefix}/%{name}
fi


%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root)
   /%{_prefix}/bin
   /%{_prefix}/usr/share

%changelog
* Wed Mar 02 2022 Hawi Stecher <hstecher@gemini.edu> 0.1.0-2
- new package built with tito


