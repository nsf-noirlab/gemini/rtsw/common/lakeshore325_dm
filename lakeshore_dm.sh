#! /bin/bash

docker run -it --rm --net=host --dns-search hi.gemini.edu --name lakeshore-dm -e EPICS_CA_ADDR_LIST=$EPICS_CA_ADDR_LIST -e DISPLAY=:0 registry.gitlab.com/nsf-noirlab/gemini/rtsw/common/gemini-ade/unstable/2022q1:latest bash -c "source /gem_base/etc/profile; pydm -m 'P=tgnirs, Q=ls325' --hide-status-bar --hide-nav-bar --hide-menu-bar --stylesheet /gem_base/usr/share/lakeshore325/styleSheet.css /gem_base/usr/share/lakeshore325/LakeShore325.ui" 
